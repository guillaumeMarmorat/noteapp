import moment from 'moment'

const state = {
  notes: [],
  hasBeenInit: false
}

const mutations = {
  INIT: (state, {notes}) => {
    state.notes = notes;
    state.hasBeenInit = true;
  },
  ADD_NOTE: (state, {note}) => state.notes.push(note),
  DEL_NOTE: (state, {note}) => {
    state.notes = state.notes.filter((statedNote) => statedNote.id !== note.id);
  },
  UPD_NOTE: (state, {note}) => {
    state.notes = state.notes.map(statedNote => (statedNote.id === note.id)? note : statedNote)
  }
}

const actions = {
  init: (store) => {
    let notes = JSON.parse(window.localStorage.getItem('notes'));
    if (notes !== null && notes !== undefined){
      notes = notes.map((note) => {
        note.created = moment(note.created);
        return note
      })

      store.commit('INIT', {notes: notes})
    } else {
      window.localStorage.setItem('notes', JSON.stringify([]));
      store.commit('INIT', {notes: []});
    }
  },
  deleteNote: (store, {note}) => {
    // Local commit
    store.commit('DEL_NOTE', {note: note});
    // Persist data
    let persistedNotes = JSON.parse(window.localStorage.getItem('notes'));
    persistedNotes = persistedNotes.filter(persistedNote => persistedNote.id !== note.id);
    window.localStorage.setItem('notes', JSON.stringify(persistedNotes));
  },
  persistNote: (store, {note}) => {
    // Commit action to the local store
    store.commit('ADD_NOTE', {note: note});
    // Persist notes within local storage
    let persistedNotes = JSON.parse(window.localStorage.getItem('notes'));
    persistedNotes.push(note)
    window.localStorage.setItem('notes', JSON.stringify(persistedNotes));
  },
  updateNote: (store, {note}) => {
    // Local commit
    store.commit('UPD_NOTE', {note: note});
    // Persist data
    let persistedNotes = JSON.parse(window.localStorage.getItem('notes'));
    persistedNotes = persistedNotes.map(persistedNote => (persistedNote.id === note.id)? note : persistedNote)
    window.localStorage.setItem('notes', JSON.stringify(persistedNotes));
  }
}

const getters = {
  getNotes: state => {
    return state.notes
  }
}

/**
 * Simple store for notes handling
 */
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
