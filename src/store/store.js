import Vue from 'vue';
import Vuex from 'vuex';
import noteModule from './module/note';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    note: noteModule,
  },
});
